# frozen_string_literal: true

require 'json'
require 'net/http'

# Fetches game data from API
class LineScores
  def index(date)
    @data = fetch(date)

    if @data['league']['games'].nil?
      []
    else
      @data['league']['games']
    end
  end

  def fetch(date)
    root_url = 'http://api.sportradar.us/mlb-t6/games/'
    api_key = ENV['SPORTSRADAR_API_KEY']

    url = URI.parse("#{root_url}#{date}/boxscore.json?api_key=#{api_key}")
    req = Net::HTTP::Get.new(url.to_s)

    res = Net::HTTP.start(url.host, url.port) do |http|
      http.request(req)
    end

    JSON.parse(res.body)
  end
end
